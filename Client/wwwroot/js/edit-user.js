﻿function getParameter(param) {
    //window.location.search: ?id=1
    var urlVar = window.location.search.substring(1);
    var urlParam = urlVar.split('=');
    if (urlParam[0] == param) {
        return urlParam[1];
    }
}

const globalUserId = getParameter("id");
$(document).ready(function () {
    //getUserInfo();
    editUserInfo();
})

function getUserInfo() {
    $.ajax({
        type: "GET",
        url: "http://localhost:5212/api/User/" + globalUserId,
        success: function (result) {
            let biggest = `
                        <div>
                            <input class="form-control" id="name" value=${result.name} />
                        </div>
                        <div>
                            <input class="form-control" id="email" required value=${result.email} />
                        </div>
                        <div>
                            <input class="form-control" id="address" required value=${result.address} />
                        </div>
                        <button id="send">Edit</button>
                `;
            $(".form-edit").html(biggest);
        },
        error: function (response) {
            console.log(response.responseText);
        }
    });
}

function editUserInfo() {
    $("#send").click(function () {
        console.log(globalUserId);
        let name = $("#name").val();
        let email = $('#email').val();
        let address = $("#address").val();
        if (!name) {
            alert("Missing name");
        } else {
            let user = {
                name: name,
                email: email,
                address: address
            };
            $.ajax({
                data: JSON.stringify(user),
                type: "POST",
                url: "http://localhost:5212/api/User/edit/" + globalUserId,
                success: function () {
                    alert("Edited successfully");
                    location.href("https://localhost:7122/user");
                },
                error: function (response) {
                    console.log(response.responseText);
                    console.log(this.url);
                }
            });
        }
    });

}