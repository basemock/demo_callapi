﻿using Microsoft.AspNetCore.Mvc;
using Server_API.Models;
using Server_API.Repositories;

namespace Server_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserRepository repo;

        public UserController(IUserRepository repo)
        {
            this.repo = repo;
        }

        [HttpGet]
        public IActionResult GetAllUser() {
            return Ok(repo.GetAllUsers());
        }

        [HttpGet("{id}")]
        public IActionResult GetUserById(int id) { 
            User? u = repo.GetUserById(id);
            if (u == null) { 
                return NotFound();
            }
            return Ok(repo.GetUserById(id));
        }

        [HttpPost("add")]
        public IActionResult AddUser(User user) {
            repo.AddUser(user);
            return Ok(user);
        }

        [HttpPost("edit/{id}")]
        public IActionResult EditUser(int id, User userToUpdate) {
            User? u = repo.GetUserById(id);
            if (u == null)
            {
                return NotFound();
            }
            repo.UpdateUser(id, userToUpdate);
            return Ok(u);
        }
    }
}
