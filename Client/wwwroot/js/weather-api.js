﻿$(document).ready(function () {
    $.ajax({
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        type: "GET",
        url: "https://localhost:7037/WeatherForecast",
        //url: "http://localhost:5212/WeatherForecast",
        success: function (result) {
            var biggest = "";
            for (let i = 0; i < result.length; i++) {
                var a = `
                    <tr>
                        <th>${result[i].date}</th>
                        <td>${result[i].temperatureC}</td>
                        <td>${result[i].summary}</td>
                    </tr>
                `;
                biggest += a;
            }
            $("#weather-display").html(biggest);

        },
        error: function (error) {
            console.log(error);
        }
    });
});