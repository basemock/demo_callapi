﻿$(document).ready(function () {
    $.ajax({
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        type: "GET",
        url: "http://localhost:5212/api/User",
        success: function (result) {
            var biggest = "";
            for (let i = 0; i < result.length; i++) {
                var a = `
                    <tr>
                        <th scope="row">${result[i].id}</th>
                        <td>${result[i].name}</td>
                        <td>${result[i].email}</td>
                        <td>${result[i].address}</td>
                    </tr>
                `;
                biggest += a;
            }
            $("#user-display").html(biggest);
            
        },
        error: function (error) {
            console.log(error);
            console.log(this.url);
        }
    });
});