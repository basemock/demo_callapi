﻿using Server_API.Models;

namespace Server_API.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly UserContext context;
        public UserRepository() { 
            context = new UserContext();
        }

        public List<User> GetAllUsers()
        {
            return context.Users.ToList();
        }

        public User GetUserById(int id)
        {
            return context.Users.FirstOrDefault(u => u.Id == id);
        }

        public void AddUser(User u)
        {
            context.Users.Add(u);   
            context.SaveChanges();
        }

        public void DeleteUser(int id)
        {
            User? usertodelete = context.Users.FirstOrDefault(u => u.Id == id);
            if (usertodelete == null) {
                throw new Exception();
            }
            context.Users.Remove(usertodelete);
            //context.SaveChanges();
        }

       
        public void UpdateUser(int id, User user)
        {
            User? userToUpdate = context.Users.FirstOrDefault(u => u.Id == id);
            if (userToUpdate == null) {
                throw new ArgumentNullException();
            }
            userToUpdate.Name = user.Name;
            userToUpdate.Email = user.Email;
            userToUpdate.Address = user.Address;

            context.Users.Update(userToUpdate);
            context.SaveChanges();
        }
       
    }
}
